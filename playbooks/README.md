# Ansible Playbooks for Infrastucture Setup

## Description
This project contains playbook for setting up various machine roles:
- Rails application server
- HAproxy load balancer
- Postgresql Database
- Gitlab Runner

## Highlight
- Most of the provisioning roles uses module-specific functionality rather
  than shell & command module to ensure idempotency is working as intended
- Roles that did not use moddule with idempotency suppport were given
  conditional guard on wheter it should be skipped or not when possible
- Gitlab runner provision playbook includes installing both postgresql and
  sqlite3 to preserve the Gemfile.lock requirement and maintains flexibility
  for the future
- When the application is deployed it will be started with the following option:
  - `-b 0.0.0.0` : set the app entry point to `0.0.0.0` and it will get auto exposed to
    outside request
  - `-d` is to set the running app as a daemon or service

## How to use
To provision infrastructure, just execute any playbook with `provision_` prefix on its name
```
$ ansible-playbook < provision script > -i inventory/hosts.yml
```

`deploy.yml` is a special playbook just for deploying the build artifact. It is built
to be executed inside Gitlab Runner instance rather than on development host machine.

To execute `deploy.yml` playbook, you need to change inventory hosts file from `inventory/hosts.yml`
to `inventory/deploy_hosts.yml`

## Configurable variable
You can configure variables in `inventory/group_vars` and in each provision playbooks
- in `all.yml` you can configure ruby, node, and postgresql
- in `app.yml` you can configure key & value of environment variable in the
  deploy servers
