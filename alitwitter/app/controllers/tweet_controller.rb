class TweetController < ApplicationController
  def index
    @tweets = Tweet.all
  end

  def create
    @tweet = Tweet.new(tweet_params)

    @tweet.save
    redirect_to '/'
  end

  def destroy
    @tweet = Tweet.find(params[:id])
    @tweet.destroy

    redirect_to '/'
  end

  private
  def tweet_params
    params.require(:tweet).permit(:message)
  end
end
