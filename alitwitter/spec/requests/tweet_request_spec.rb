require 'rails_helper'

RSpec.describe 'Tweets', type: :request do
  describe 'GET /index' do
    it 'returns http success' do
      get '/tweet/index'
      expect(response).to have_http_status(:success)
    end
  end

  describe 'POST /tweet' do
    it 'creates a tweet and redirect to index' do
      post '/tweet', params: { tweet: { message: 'hello' } }

      expect(response).to redirect_to('/')
      follow_redirect!

      expect(response.body).to match(/hello/)
    end
  end

  describe 'DELETE /tweet' do
    it 'should delete created post and redirect to index' do
      post '/tweet', params: { tweet: { message: 'hello' } }

      delete "/tweet/#{assigns(:tweet).id}"

      expect(response).to redirect_to('/')
    end
  end
end
