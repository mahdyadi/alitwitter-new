require 'rails_helper'

RSpec.describe TweetController, type: :controller do
  describe '#create' do
    context 'given valid input' do
      context 'when form data received' do
        it 'should render index page' do
          post :create, params: { tweet: { message: 'any' } }

          expect(response).to redirect_to('/')
        end
      end
    end
  end
end
