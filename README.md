# AliTwitter App with Virtual Machine based infrastructure
## Description
This is a project repository for Gojek's bootcamp 006
devops module. Included in this repository are:

- Web application (alitwitter) with accompanying Gitlab CI script

- Vagrant boxes setup

- Ansible playbooks for configuring VMs

- Ansible playbooks used by Gitlab Runner instance to deploy the app

The aim of this project is to demonstrate CI/CD flow using virtual machine-based
infrastructure, from code change to deployed app.

Most of the infrastructure setup is automated provided you have the necessary tool installed.
For more information, you can refer to the `Infrastructure setup` part below.

Continuous Integration(CI) and Continous Deployment(CD) flow on top of the prepared
infrastructure is achieved using Gitlab CI features and Ansible playbooks.
Gitlab runner instance will handle the test, build, and deployment process
based on the script specified in `.gitlab-ci.yml` and `deploy` Ansible playbook

## Project organization
- The web application folder is located in `alitwitter` directory
- Provisioning and deployment playbooks is grouped inside  `playbooks` directory

App folder and playbooks folder is separated rather than merged to ease
navigation and declutter files and folders that have different usage.

## Usage requirement
This project was developed with help of the following tools:
- `Ansible 2.9` or greater
- `Vagrant 2.2.7`
- `VirtualBox` as Vagrant provider

You can install Ansible, Vagrant, and VirtualBox in your local computer
to use this project.

This project is tested and developed on MacOS 10.15.3 Catalina. Compatibility
when used with different setup is not guaranteed.

## How to use
Please follow all instruction described below step-by-step to ensure
a working infrastructure and application is achieved

### Gitlab Repository Setup

1. Create new git repository and checkout all files and folder in this project
    ```
    $ git init
    $ git add .
    $ git commit
    ```

2. Then you can create new Gitlab repository and get the repository's private runner
    `registration token` via the Gitlab UI. Copy and paste the value into the runner
    provisioning playbook `gitlab_regis_token` variable in `playbooks/provision_runner.yml`
    ```yaml
    ---
    - name: Setup shell runner
      hosts: gitlab_runner
      ...
      vars:
        ...
        gitlab_regis_token: # place the token here
      roles:
        ...
    ```

3. You also need to set Gitlab CI/CD Variables for the following key/value:

    | Variable Key               | Variable value                                                       |
    |----------------------------|----------------------------------------------------------------------|
    | `RAILS_MASTER_KEY`         | Value of from `master.key` file, see alitwitter readme for more info |
    | `RUNNER_DATABASE_USERNAME` | Test database username used in the runner                            |
    | `RUNNER_DATABASE_PASSWORD` | Test database user passsword used in the runner                      |

    both `RUNNER_DATABASE_USERNAME` and `RUNNER_DATABASE_PASSWORD` value can be copied from
    `postgresql.user` and `postgresql.user_password` value from `playbooks/inventory/group_vars/all.yml`,
    which is the credentials of database of the deployment environment. Currently no distinction is made
    for test database user and production database user identity because both credentials would still
    be exposed in this project.

    An example and matching `master.key` file for current app is included in the root of the project
    named `default.master.key`. You can use this master key if you did not want to generate
    a new one.

Add the repository into this project's git repository remote as origin or other
remote name of your choice

### Infrastructure setup
1. Setup virtual machine boxes/instance.

    Make sure Vagrant is installed and VirtualBox is set as its provider(or other provider of your choice),
    then initialize Vagrant boxes using the `Vagrantfile` provided by this project in project root directory.
    It is highly advised that you do this step in the project root directory.

    There are 5 machines defined in this project:
    - `database` : Machine to be set as database service using PostgreSQL
    - `load-balancer` : Machine to be set as load balancer node using HAProxy
    - `gitlab-runner` : Machine to be set as Gitlab Runner instance to enable CI/CD flow
    - `app1` & `app2` : Machines to be set as application servers. Two instances is created to
    simulate load-balancing feature

    example commands:
    ```
    $ cd < this project path >
    $ vagrant up
    ```

2. Setup playbook configuration for Ansible
    Get the ssh key for each machines/boxes by calling `vagrant ssh-config` and take note of each
    machine IdentityFile locations

    example:
    ```
    $ vagrant ssh-config
    Host database
      ...
      IdentityFile /Users/drianka/devops_vm_assessment/.vagrant/machines/database/virtualbox/private_key
      ...

    Host load-balancer
      ...
      IdentityFile /Users/drianka/devops_vm_assessment/.vagrant/machines/load-balancer/virtualbox/private_key
      ...

    ...
    ```

    Then modify the `ansible_ssh_private_key_file` for each host in `playbooks/inventory/hosts.yml` file based on the path to each vagrant boxes IdentityFile with the following mapping of Vagrant host names to Ansible host name:

    | Vagrant host    | Ansible host    |
    |-----------------|-----------------|
    | `database`      | `database`      |
    | `load-balancer` | `load_balancer` |
    | `gitlab-runner` | `gitlab_runner` |
    | `app1`          | `app1`          |
    | `app2`          | `app2`          |

    example:
    ```yaml
    ---
    all:
      hosts:
        gitlab_runner:
          ...
          ansible_ssh_private_key_file: # place gitlab-runner IdentityFile path here
          ...

        database:
          ...
          ansible_ssh_private_key_file: # place database IdentityFile path here
          ...

        ...

      children:
        app:
          hosts:
            app1:
              ...
              ansible_ssh_private_key_file: # place app1 IdentityFile path here
              ...

            ...
    ```


3. Setup boxes machine using provisioning playbooks

    Make sure Ansible is installed, and you can just execute the main provisioning playbook `playbooks/provision.yml`
    with inventory hosts file `playbooks/hosts.yml`

    example commands:
    ```
    $ ansible-playbook playbooks/provision.yml -i playbooks/hosts.yml
    ```

    Or if you want to do the setup separately, you can use the following playbooks:

    | Ansible host    | Provisioning playbook                   |
    |-----------------|-----------------------------------------|
    | `database`      | `playbooks/provision_db.yml`            |
    | `load-balancer` | `playbooks/provision_load_balancer.yml` |
    | `gitlab-runner` | `playbooks/provision_gitlab_runner.yml` |
    | `app1`          | `playbooks/provision_app.yml`           |
    | `app2`          | `playbooks/provision_app.yml`           |

    Wait until the setup is done. Make sure you have stable internet connection
    to ensure no error raised by inability to fetch packages from the remote source

### Triggering CI/CD Flow
All step of CI/CD flow is written in the `.gitlab-ci.yml` file in the project root
directory.

You just need ensure you private runner is ready to accept jobs and this project's
remote git repository is already set

Push code change to the remote repository to trigger test, build, and deployment
step

## What does the .gitlab-ci.yml do?
It handles testing the application after code change, building and packaging the app
into deployable artifact, and deploying the artifact in the designated servers.

### Highlights:
- Application is tested in `test` environment, then build and packaged in `production` environment
- Test coverage folders is outputted as artifacts in `test` stage
- Testing is done using runner's local postgres database rather than sqlite3
  as an effort to mirror production setup in testing phase
- packaged app in `tar` archive is outputted as artifacts in `build` stage
- bundler is configured to use `deployment` keyword in each job to install gems inside
  `vendor/bundle` folder instead of system-wide default gem location

## More information
See more focused readme files in relevant subdirectories:
- `playbooks/README.md` for documentation on playbooks
- `alitwitter/README.md` for documentation on the app

This project README file is split to better separate concerns
